# Barebone Sass
A lightweight and skeletal website boilerplate. Avoids bloated and opinionated code of many other front-end boilerplates/frameworks. Built as a starting point for projects to save the initial legwork, encouraging rapid development.

## Overview
* Built with Sass
* Uses BEM syntax
* Closely follows [CSS Guidelines](http://cssguidelin.es/) styleguide
* HTML5 semantics
* WAI-ARIA roles
* Minimal HTTP requests for high-end web performance
* Task automisation via Grunt:
  * Sass compiling
  * BrowserSync for autoloading in browser and watching for file changes
  * CSS/Sass and JS minification, linting and watching
  * Image optimisation
  * Performance testing with PerfBudget
* .editorconfig file to maintain consistent coding styles between different editors 

## Installation

### Dependencies
* [NodeJS](http://nodejs.org/download/)
* [Grunt](http://gruntjs.com/installing-grunt)

## Browser support
As defined in the grunt-autoprefixer plugin config `/grunt/autoprefixer.js`.

* Last 2 versions of all main browsers
* IE8+

## Quick CSS/Sass styleguide

See `docs/styleguide` for details. General style guidelines include:

* Use [BEM naming convention](http://cssguidelin.es/#bem-like-naming)
* Avoid using ID's
* Create a new class for JS hooks, prefixed with `js-`. E.g. `js-btn`. [Further reading](http://cssguidelin.es/#javascript-hooks).
* Avoid tying objects to page or location on the page where possible. E.g. use `.nav-primary` instead of `.header-nav`. See further details on [location independence](http://cssguidelin.es/#location-independence)
* Avoid [overly specific selectors](http://cssguidelin.es/#specificity). E.g. use `.nav-primary__link` for navigation list anchors instead of `.nav-primary a` or even `.nav-primary > li > a`
* Use [single responsibility](http://csswizardry.com/2012/04/the-single-responsibility-principle-applied-to-css/), e.g. `class="btn btn--primary"` instead of `class="button--primary"`. Where not applicable, could use a [placeholder selector](http://csswizardry.com/2014/01/extending-silent-classes-in-sass/)
* Avoid nesting BEM-like selectors to [avoid unsearchable selectors](http://www.sitepoint.com/beware-selector-nesting-sass/#unsearchable-selectors).

## Roadmap
* ~~Add Browserify to modularise JavaScript~~
* ~~Add styleguide generator - KSS~~
* ~~Add vertical rhythm~~
* ~~Add an .editorconfig file~~
* ~~Add SVG to PNG conversion in Grunt~~
* Add a responsive (fluid) grid - Susy 2
* Configure concurrent Grunt tasks