module.exports = {

  dist: {
    src: 'assets/images/sprites/*.png',
    destImg: 'assets/images/sprites.png',
    destCSS: 'css/sass/components/_sprites.scss',
    algorithm: 'binary-tree',
    cssFormat: 'scss',
    'cssOpts': {
      'cssClass': function (item) {
        return '.sprite-' + item.name; // CSS classname
      }
    }
  }

};