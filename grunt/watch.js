module.exports = {

  css: {
    files: ['css/**/*.scss'],
    tasks: ['sass', 'autoprefixer', 'cssmin']
  },

  scripts: {
    files: ['js/modules/*.js', 'js/main.js'],
    tasks: ['jshint', 'uglify']
  }

};