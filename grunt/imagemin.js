module.exports = {
  options: {
    optimizationLevel: 7 // levels between 0 and 7
  },

  dist: {
    files: [{
      expand: true,
      cwd: 'assets/images/',
      src: ['**/*.{png,jpg,gif,svg}'],
      dest: 'assets/images/'
    }]
  }
};