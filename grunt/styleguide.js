module.exports = {

  options: {
      template: {
        src: 'assets/vendor/styleguide-template' // https://github.com/htanjo/kss-node-template
      },
      framework: {
        name: 'kss',
        options: {
          includeType: 'css',
          includePath: 'public/css/application.css'
        }
      }
    },
  all: {
    files: {
      'docs/styleguide': 'css/*.css'
    }
  }

};