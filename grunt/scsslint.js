module.exports = {

  allFiles: ['css/sass/**/*.scss'],
  options: {
    bundleExec: false,
    config: '.scss-lint.yml',
    reporterOutput: null, //'scss-lint-report.xml'
    colorizeOutput: true,
    maxBuffer: NaN
  }

};