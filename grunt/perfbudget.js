module.exports = {

  default: {
    options: {
      url: 'http://www.google.co.uk', // external URL required unless private instance of WPT
      key: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', // 32 char key
      budget: {
        visualComplete: '4000',
        speedIndex: '1500'
      }
    }
  }

};