module.exports = {
  options: {
    browsers: ['last 2 versions', 'ie >= 8']
  },

  multiple_files: {
    expand: true,
    flatten: true,
    src: 'css/*.css',
    dest: 'css/'
  }
};