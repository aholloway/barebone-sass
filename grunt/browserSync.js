module.exports = {
  bsFiles: {
    src : [
      "css/build/*.css",
      "*.html"
    ]
  },
  options: { // http://www.browsersync.io/docs/options/
    server: {
      baseDir: "./"
    },
    watchTask: true
  }
};