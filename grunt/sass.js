module.exports = {
  dist: {
    options: {
      style: 'expanded',
      debugInfo: true,
      sourcemap: true
    },
    files: {
      'css/style.css': 'css/scss/style.scss'
    }
  }
};