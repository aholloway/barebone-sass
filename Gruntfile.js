module.exports = function(grunt) {

  // Measures the time each task takes
  require('time-grunt')(grunt);

  // Load grunt config
  require('load-grunt-config')(grunt, {
    jitGrunt: true
  });

  // Load all grunt tasks
  require('load-grunt-tasks')(grunt);

  // Tasks
  grunt.registerTask('default', ['sass', 'scsslint', 'cssmin', 'jshint', 'browserify', 'uglify', 'newer:imagemin', 'newer:svg2png', 'newer:sprite', 'browserSync', 'watch']);

  grunt.registerTask('docs', ['styleguide']); // Create styleguide using KSS

  grunt.registerTask('perf', ['perfbudget']); // Run performance test using WebPageTest
};